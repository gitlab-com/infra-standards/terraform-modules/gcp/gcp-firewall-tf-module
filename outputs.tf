###############################################################################
# Google Ingress Firewall Rule Configuration Module
# -----------------------------------------------------------------------------
# outputs.tf
###############################################################################

output "firewall" {
  description = "Firewall rule configuration"
  value = {
    description = google_compute_firewall.ingress_compute_firewall.description
    direction   = google_compute_firewall.ingress_compute_firewall.direction
    disabled    = google_compute_firewall.ingress_compute_firewall.disabled
    id          = google_compute_firewall.ingress_compute_firewall.id
    name        = google_compute_firewall.ingress_compute_firewall.name
    priority    = google_compute_firewall.ingress_compute_firewall.priority
    self_link   = google_compute_firewall.ingress_compute_firewall.self_link
  }
}

output "logging" {
  description = "GCP Logging configuration"
  value = {
    enabled = var.logging_enabled
  }
}

output "network" {
  description = "GCP Network configuration"
  value = {
    self_link = google_compute_firewall.ingress_compute_firewall.network
  }
}

output "rules" {
  description = "Allow and deny rules for ports and protocols"
  value = {
    "allow" = length(var.rules_allow) > 0 ? zipmap(google_compute_firewall.ingress_compute_firewall.allow[*]["protocol"], google_compute_firewall.ingress_compute_firewall.allow[*]["ports"]) : null
    "deny"  = length(var.rules_deny) > 0 ? zipmap(google_compute_firewall.ingress_compute_firewall.deny[*]["protocol"], google_compute_firewall.ingress_compute_firewall.deny[*]["ports"]) : null
  }
}

output "source" {
  description = "Source that traffic is coming from that this firewall rule should apply to"
  value = {
    ranges = google_compute_firewall.ingress_compute_firewall.source_ranges
    tags   = var.source_tags != null ? google_compute_firewall.ingress_compute_firewall.source_tags : null
  }
}

output "target" {
  description = "Destination resources that this firewall rule should apply to"
  value = {
    tags = google_compute_firewall.ingress_compute_firewall.target_tags
  }
}
